<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('list/{collection}', 'HomeController@listSurvey');

Route::get('add', function () {
    return view('add');
});

Route::post('survey', 'HomeController@addSurvey')->name('add-survey')->middleware('auth');
Route::get('verify-house', 'HomeController@verifyhouse')->middleware('auth');
Route::get('getSurvey', 'HomeController@getSurvey')->middleware('auth');

Route::get('survey', function (\Illuminate\Http\Request $request) {
    if ($request->get('house_no')) {
        $survey = \App\Survey::where('house_no', $request->house_no)->get()->first();
        if ($survey == null) {
            $new_survey = new \App\Survey($request->all());
            $new_survey->house_no = $request->house_no;
            $new_survey->save();
        }
    }
    return view('survey');
})->middleware('auth');

Route::get('details/{id}', 'HomeController@details')->middleware('auth');

Auth::routes();



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
