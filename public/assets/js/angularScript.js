var app = angular.module('myApp', []);

//app.config(['$locationProvider', function ($locationProvider) {
//    $locationProvider.html5Mode(true);
//}]);
app.controller('AppController', function ($scope, $http,$window,$location) {

    $scope.survey = {};
    $scope.survey.family_members = [{}];

    $scope.addRow = function () {
        $scope.survey.family_members.push({});
    };

    $scope.removeRow = function (index) {
        $scope.survey.family_members.splice(index, 1);
    };
    var addUrl = $location.absUrl();
    console.log();

    $http.get('/getSurvey?' + addUrl.split('?')[1])
        .then(function success(data) {
            $scope.survey = data.data;
        }, function error(error) {
            console.log(error);
        });


    $scope.addSurvey = function () {
        var url = $location.absUrl();
        $http.post(url, $scope.survey)
            .then(function success(data) {
                alert('Data saved');
                $window.location.href = '/add';
            }, function error(error) {
                console.log(error);
                $scope.validationErrors = error.data;
                console.log($scope.validationErrors);

            });
    };


    $scope.found = false;
    $scope.house_no = '';
    $scope.verify = function (house_no) {
        $http.get('verify-house?house=' + house_no)
            .then(function success(data) {
                if(data.status == 202 && data.data.error == 'not-found') {
                    $window.location.href = '/survey?house_no=' + house_no;
                }else{
                    $scope.foundUrl = '/survey?house_no=' + house_no;
                    $scope.found = true;
                }
            }, function error(error) {
                console.log(error);
            });
    };

    $scope.clearForm = function () {
        $scope.house_no = '';
        $scope.found = false;
    };

    $scope.houseNoExist = function () {
        $window.location.href = '/survey?house_no=' + $scope.house_no;
    };


    $scope.cancelSurvey = function () {
        $window.location.href = '/add';
    };

});