<!doctype html>
<html class="no-js " lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="IUML Maithra Unit | The Indian Union Muslim League is a political party in India.">
    <title>IUML Maithra Unit Committee</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Favicon-->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
    <script src="assets/js/angular.js"></script>
    <script src="assets/js/angularScript.js"></script>
</head>
<body class="theme-orange" ng-controller="AppController">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">        
            <div class="line"></div>
    		<div class="line"></div>
    		<div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"><img src="assets/img/iuml.png" width="48" height="48" alt="IUML Maithra Unit Committee"></div>
        </div>
    </div>

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="col-12">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">IUML Maithra</a>
            </div>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="sign-in.html" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a></li>
            </ul>
        </div>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        @include('app.navigation')
    </aside>
    <section class="content">
       <div class="block-header">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Add New Survey
                    <small class="text-muted">Welcome to IUML Maithra Unit Committee Survey Application</small>
                    </h2>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h2 class="card-inside-title"> Add New new thread here...</h2>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <b>Please enter valid house number</b>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" ng-model="house_no" name="house_no"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button ng-click="verify(house_no)" type="submit" class="btn btn-raised btn-primary waves-effect col-lg-12">verify</button>
                        </div>
                        <div class="body" ng-if="found">
                            <div class="alert alert-warning">
                                <strong>Oh snap!</strong> This entry already created...<br/> Can you edit this entry ?
                                <div class="button-demo m-t-30">
                                    <button type="button" class="btn btn-raised btn-default waves-effect" ng-click="clearForm()">NO</button>
                                    <button ng-click="houseNoExist()" type="button" class="btn  btn-raised btn-info waves-effect">YES</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </section>

    <!-- Jquery Core Js --> 
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
</body>
</html>