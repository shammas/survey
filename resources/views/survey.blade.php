<!doctype html>
<html class="no-js " lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="IUML Maithra Unit | The Indian Union Muslim League is a political party in India.">
    <title>IUML Maithra Unit Committee</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Favicon-->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <script src="assets/js/angular.js"></script>
    <script src="assets/js/angularScript.js"></script>
</head>
<body class="theme-orange"  ng-controller="AppController">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
    		<div class="line"></div>
    		<div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"><img src="assets/img/iuml.png" width="48" height="48" alt="IUML Maithra Unit Committee"></div>
        </div>
    </div>

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="col-12">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">IUML Maithra</a>
            </div>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="sign-in.html" class="mega-menu" data-close="true"><i class="zmdi zmdi-power"></i></a></li>
            </ul>
        </div>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        @include('app.navigation')
    </aside>
    <section class="content">
       <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Add New Survey
                    <small class="text-muted">Welcome to IUML Maithra Unit Committee Survey Application</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        {{--<form action="" method="POST" ng-submit="addSurvey()">--}}
                        <form action="{{route('add-survey')}}" method="POST">
                            {{csrf_field()}}
                            <div class="body">
                                <h2 class="card-inside-title"> Family head details for  house No = {{\Illuminate\Support\Facades\Request::get('house_no')}}</h2>
                                <div class="row clearfix">

                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Name of Head of the Family*</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="family_head_name" ng-model="survey.family_head_name" value="{{old('family_head_name',optional((isset($survey) ? $survey : null))->family_head_name)}}"/>
                                                @error('family_head_name')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.family_head_name">@{{ validationErrors.family_head_name[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Surname of the Family (In local)</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="sure_name" ng-model="survey.sure_name" value="{{old('sure_name',optional((isset($survey) ? $survey : null))->sure_name)}}"/>
                                                @error('sure_name')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.sure_name">@{{ validationErrors.sure_name[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Age</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="age" ng-model="survey.age" value="{{old('age',optional((isset($survey) ? $survey : null))->age)}}"/>
                                                @error('age')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.age">@{{ validationErrors.age[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Gender</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="gender" ng-model="survey.gender">
                                                    <option value="">-- Please select --</option>
                                                    <option value="male" {{ old('gender',optional((isset($survey) ? $survey : null))->gender) == 'male' ? 'selected' : '' }} >Male</option>
                                                    <option value="female" {{ old('gender',optional((isset($survey) ? $survey : null))->gender) == 'female' ? 'selected' : '' }}>Female</option>
                                                    <option value="others" {{ old('gender',optional((isset($survey) ? $survey : null))->gender) == 'others' ? 'selected' : '' }}>Others</option>
                                                </select>
                                                @error('gender')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.gender">@{{ validationErrors.gender[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Occupation</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                {{--<input type="text" class="form-control" name="occupation" ng-model="survey.occupation" value="{{old('occupation',optional((isset($survey) ? $survey : null))->occupation)}}"/>--}}
                                                <select class="form-control show-tick" ng-model="survey.occupation">
                                                    <option value="">-- Please select --</option>
                                                    <option value="coolie-worker">Coolie Worker</option>
                                                    <option value="nri-pravasi">NRI Pravasi</option>
                                                    <option value="govt-servant">Govt Servant</option>
                                                    <option value="business-man">Business Man</option>
                                                    <option value="professionals">Professionals</option>
                                                    <option value="studying">Studying</option>
                                                    <option value="jobless">Jobless</option>
                                                </select>
                                                @error('occupation')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.occupation">@{{ validationErrors.occupation[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Political Party</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="political_party" ng-model="survey.political_party">
                                                    <option value="">-- Please select --</option>
                                                    <option value="iuml" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'iuml' ? 'selected' : '' }}>IUML</option>
                                                    <option value="iuml-black" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'iuml-black' ? 'selected' : '' }}>IUML-Black</option>
                                                    <option value="inc" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'inc' ? 'selected' : '' }}>INC</option>
                                                    <option value="cpi" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'cpi' ? 'selected' : '' }}>CPI</option>
                                                    <option value="cpm" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'cpm' ? 'selected' : '' }}>CPM</option>
                                                    <option value="ncp" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'ncp' ? 'selected' : '' }}>NCP</option>
                                                    <option value="bjp" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'bjp' ? 'selected' : '' }}>BJP</option>
                                                    <option value="ncp" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'ncp' ? 'selected' : '' }}>NCP</option>
                                                    <option value="others" {{ old('gender',optional((isset($survey) ? $survey : null))->political_party) == 'others' ? 'selected' : '' }}>Others</option>
                                                </select>
                                                @error('political_party')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.political_party">@{{ validationErrors.political_party[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Blood Group</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="blood_group" ng-model="survey.blood_group">
                                                    <option value="">-- Please select --</option>
                                                    <option value="a+" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'a+' ? 'selected' : '' }}>A+</option>
                                                    <option value="o+" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'o+' ? 'selected' : '' }}>O+</option>
                                                    <option value="b+" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'b+' ? 'selected' : '' }}>B+</option>
                                                    <option value="ab+" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'ab+' ? 'selected' : '' }}>AB+</option>
                                                    <option value="a-" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'a-' ? 'selected' : '' }}>A-</option>
                                                    <option value="o-" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'o-' ? 'selected' : '' }}>O-</option>
                                                    <option value="b-" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'b-' ? 'selected' : '' }}>B-</option>
                                                    <option value="ab-" {{ old('gender',optional((isset($survey) ? $survey : null))->blood_group) == 'ab-' ? 'selected' : '' }}>AB-</option>
                                                </select>
                                                @error('blood_group')
                                                    <span class="error">ffffff</span>
                                                @enderror
                                                <span ng-if="validationErrors.blood_group">@{{ validationErrors.blood_group[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Phone Number</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="phone" ng-model="survey.phone" value="{{old('phone',optional((isset($survey) ? $survey : null))->phone)}}"/>
                                                <span ng-if="validationErrors.phone">@{{ validationErrors.phone[0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h2 class="card-inside-title"> Family members details</h2>
                                <div class="row" ng-repeat="family in survey.family_members">
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Member Name</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" ng-model="family.name" name="family[@{{$index}}][name]"/>
                                                <span ng-if="validationErrors['family.'+$index+'.name']">@{{ validationErrors["family_members."+$index+".name"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-sm-12">
                                        <b>Gender</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" ng-model="family.gender" name="family[@{{$index}}][gender]">
                                                    <option value="">-- Please select --</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                    <option value="others">Others</option>
                                                </select>
                                                <span ng-if="validationErrors['family.'+$index+'.gender']">@{{ validationErrors["family_members."+$index+".gender"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-sm-12">
                                        <b>Age</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" ng-model="family.age" name="family[@{{$index}}][age]"/>
                                                <span ng-if="validationErrors['family.'+$index+'.age']">@{{ validationErrors["family_members."+$index+".age"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-6 col-sm-12">
                                        <b>Blood Group</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" ng-model="family.blood_group" name="family[@{{$index}}][blood_group]">
                                                    <option value="">-- Please select --</option>
                                                    <option value="a+">A+</option>
                                                    <option value="o+">O+</option>
                                                    <option value="b+">B+</option>
                                                    <option value="ab+">AB+</option>
                                                    <option value="a-">A-</option>
                                                    <option value="o-">O-</option>
                                                    <option value="b-">B-</option>
                                                    <option value="ab-">AB-</option>
                                                </select>
                                                <span ng-if="validationErrors['family.'+$index+'.blood_group']">@{{ validationErrors["family_members."+$index+".blood_group"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Relationship</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" ng-model="family.relationship" name="family[@{{$index}}][relationship]">
                                                    <option value="">-- Please select --</option>
                                                    <option value="spouse">Spouse</option>
                                                    <option value="daughter">Daughter</option>
                                                    <option value="son">Son</option>
                                                    <option value="sister">Sister</option>
                                                    <option value="brother">Brother</option>
                                                </select>
                                                <span ng-if="validationErrors['family.'+$index+'.relationship']">@{{ validationErrors["family_members."+$index+".relationship"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Occupation</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" ng-model="family.occupation" name="family[@{{$index}}][occupation]">
                                                    <option value="">-- Please select --</option>
                                                    <option value="coolie-worker">Coolie Worker</option>
                                                    <option value="nri-pravasi">NRI Pravasi</option>
                                                    <option value="govt-servant">Govt Servant</option>
                                                    <option value="business-man">Business Man</option>
                                                    <option value="professionals">Professionals</option>
                                                    <option value="studying">Studying</option>
                                                    <option value="jobless">Jobless</option>
                                                </select>
                                                <span ng-if="validationErrors['family.'+$index+'.occupation']">@{{ validationErrors["family_members."+$index+".occupation"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Voting Status</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input data-ng-model="family.voting_status" name="family[@{{$index}}][voting_status]" data-ng-value="'matured'" type="radio" class="with-gap" id="radio_@{{ $index }}3" ng-checked="family.voting_status == ''matured'"/>
                                                <label for="radio_@{{ $index }}3">Matured</label>
                                                <input data-ng-model="family.voting_status" name="family[@{{$index}}][voting_status]" data-ng-value="'new-voter'" type="radio" id="radio_@{{ $index }}4" class="with-gap" ng-checked="family.voting_status == 'new-voter'" />
                                                <label for="radio_@{{ $index }}4">New Voter</label>
                                                <input data-ng-model="family.voting_status" name="family[@{{$index}}][voting_status]" data-ng-value="'no'" type="radio" id="radio_@{{ $index }}5" class="with-gap" ng-checked="family.voting_status == 'no'" />
                                                <label for="radio_@{{ $index }}5">No </label>
                                            </div>
                                            <span ng-if="validationErrors['family.'+$index+'.voting_status']">@{{ validationErrors["family_members."+$index+".voting_status"][0]}}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12" ng-if="family.voting_status != 'no'">
                                        <b>Political Party</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" ng-model="family.political_party" name="family[@{{$index}}][political_party]">
                                                    <option value="">-- Please select --</option>
                                                    <option value="iuml">IUML</option>
                                                    <option value="iuml-black">IUML-Black</option>
                                                    <option value="inc">INC</option>
                                                    <option value="cpi">CPI</option>
                                                    <option value="cpm">CPM</option>
                                                    <option value="ncp">NCP</option>
                                                    <option value="bjp">BJP</option>
                                                    <option value="ncp">NCP</option>
                                                    <option value="others">Others</option>
                                                </select>
                                                <span ng-if="validationErrors['family.'+$index+'.political_party']">@{{ validationErrors["family_members."+$index+".political_party"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Phone Number</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" ng-model="family.phone" name="family[@{{$index}}][phone]"/>
                                                <span ng-if="validationErrors['family.'+$index+'.phone']">@{{ validationErrors["family_members."+$index+".phone"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12" ng-if="family.occupation == 'studying'">
                                        <b>School / Collage Name</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" ng-model="family.school" name="family[@{{$index}}][school]"/>
                                                <span ng-if="validationErrors['family.'+$index+'.school']">@{{ validationErrors["family_members."+$index+".school"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12" ng-if="family.occupation == 'studying'">
                                        <b>Studied In</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" ng-model="family.study_in" name="family[@{{$index}}][study_in]"/>
                                                <span ng-if="validationErrors['family.'+$index+'.study_in']">@{{ validationErrors["family_members."+$index+".study_in"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12" ng-if="family.occupation == 'studying'">
                                        <b>Madrasa</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select class="form-control show-tick" ng-model="family.madrasa" name="family[@{{$index}}][madrasa]">
                                                    <option value="">-- Please select --</option>
                                                    <option value="ek">EK Madrasa</option>
                                                    <option value="ap">AP Madrasa</option>
                                                    <option value="others">Others</option>
                                                    <option value="not-going">Not Going</option>
                                                </select>
                                                <span ng-if="validationErrors['family.'+$index+'.madrasa']">@{{ validationErrors["family_members."+$index+".madrasa"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <b>Note</b>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" ng-model="family.note" name="family[@{{$index}}][note]"/>
                                                <span ng-if="validationErrors['family.'+$index+'.note']">@{{ validationErrors["family_members."+$index+".note"][0]}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="body">
                                <div class="button-demo">
                                    <button type="button"  class="btn half btn-raised btn-default waves-effect" ng-click="cancelSurvey()">Cancel</button>
                                    <button type="button" class="btn half  btn-raised btn-success waves-effect" ng-click="addRow()">Add person</button>
                                    <button type="button" class="btn full btn-raised btn-primary waves-effect" ng-click="addSurvey(survey)">Complete Survey</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
    <script src="assets/js/pages/forms/basic-form-elements.js"></script>

    <script>
        function addPerson(){
        }
    </script>
</body>
</html>