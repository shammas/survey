<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="IUML Maithra Unit | The Indian Union Muslim League is a political party in India.">
    <title>IUML Maithra Unit Committee</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">

    <!-- Favicon-->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <!-- JQuery DataTable Css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">
</head>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"><img src="assets/img/iuml.png" width="48" height="48" alt="IUML Maithra Unit Committee"></div>
        </div>
    </div>

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="col-12">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">IUML Maithra</a>
            </div>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @include('app.logout')
            </ul>
        </div>
    </nav>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        @include('app.navigation')
    </aside>
    <section class="content">
       <div class="block-header">
            <div class="row">
                <div class="col-lg-12">
                    <h2>All Members Family wise Survey's
                    <small class="text-muted">Welcome to IUML Maithra Unit Committee Survey Application</small>
                    </h2>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>House No</th>
                                            <th>Surname</th>
                                            <th>F.Head.Name</th>
                                            <th>Party</th>
                                            <th>Phone Number</th>
                                            <th>Madrasa</th>
                                            <th>Age</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>House No</th>
                                            <th>Surname</th>
                                            <th>F.Head.Name</th>
                                            <th>Party</th>
                                            <th>Phone Number</th>
                                            <th>Madrasa</th>
                                            <th>Age</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                     @forelse ($surveys as $survey)
                                        <tr>
                                            <td><a href="{{url('details',$survey->id)}}">{{$survey->house_no}}</a></td>
                                            <td><a href="{{url('details',$survey->id)}}">{{$survey->sure_name}}</a></td>
                                            <td>{{$survey->family_head_name}}</td>
                                            <td>{{$survey->political_party}}</td>
                                            <td>{{$survey->phone}}</td>
                                            <td>{{$survey->madrasa}}</td>
                                            <td>{{$survey->age}}</td>
                                        </tr>
                                    @empty
                                        <p>No Data</p>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
    <script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>

    <script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->
    <script src="{{asset('assets/js/pages/tables/jquery-datatable.js')}}"></script>

</body>
</html>