<?php

namespace App\Http\Controllers;
ini_set("xdebug.var_display_max_children", -1);
ini_set("xdebug.var_display_max_data", -1);
ini_set("xdebug.var_display_max_depth", -1);
use App\FamilyMember;
use App\Survey;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $surveys = Survey::with('family_members')->get();
        return view('dashboard', compact('surveys'));
    }


    public function listSurvey($collection)
    {
        if ($collection == 'all') {
            $surveys = Survey::with('family_members')->get();
        }

        if ($collection == 'total') {
            $surveys = [];
//            $survey = Survey::all()->toArray();
//            $member = FamilyMember::with('survey')->get()->toArray();
//            array_push($survey, $member);
//            var_dump($survey);
//            exit;
//            return view('total', compact('survey'));
        }

        if ($collection == 'new-voter') {
            $surveys = Survey::with('family_members')->whereHas('family_members',function ($q) {
                $q->where('voting_status', 'new-voter');
            })->get();
        }
        if ($collection == 'bloodbank') {
            $surveys = FamilyMember::with('survey')->get();
            return view('list-blood', compact('surveys'));
        }
        return view('list',compact('surveys'));
    }


    public function addSurvey(Request $request)
    {

//        $request->validate([
//            'family_members.*.name' => 'required',
//        ]);

        $validator = Validator::make($request->all(), [
            'house_no' => 'required',
            'family_head_name' => 'required',
            'family_members.*.name' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 500);
        }

        if ($request->has('house_no')) {
            $survey = Survey::where('house_no', $request->get('house_no'))->get()->first();
            $survey->fill($request->except('_token', 'family'));
            if ($survey->save()) {
                foreach ($request->family_members as $family) {
                    if (isset($family['id'])) {
                        $familytmp = FamilyMember::find($family['id']);
                        $familytmp->fill($family);
                        $familytmp->update();
                    }else{
                        $family_member = new FamilyMember($family);
                        $family_member->survey_id = $survey->id;
                        $family_member->save();
                    }
                }

                return Response::json($survey);
            }

        }else{
            return Response::json(['error' => 'house no not found'], 500);
        }
    }


    public function verifyhouse(Request $request)
    {
        if ($request->has('house')) {
            $survey = Survey::where('house_no', $request->house)->get()->first();
            if($survey != null)
                return $survey;
            else
                return Response::json(['error' => 'not-found'],202);
        }else{
            return Response::json(['error' => 'Validation error'], 400);
        }
    }

    public function getSurvey(Request $request)
    {
        if ($request->has('house_no')) {
            $survey = Survey::where('house_no', $request->get('house_no'))->with('family_members')->get()->first();
            if ($survey != null) {
                return $survey;
            }else{
                return Response::json(['error' => 'not-found'], 400);
            }
        }
    }


    public function details($id)
    {
        $survey = Survey::where('id', $id)->with('family_members')->get()->first();
        return view('viewDetails', compact('survey'));
    }
}
