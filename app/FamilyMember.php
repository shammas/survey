<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyMember extends Model
{
    protected $fillable = [
        'name', 'gender', 'age', 'blood_group', 'relationship',
        'occupation', 'voting_status', 'political_party', 'phone', 'school', 'study_in',
        'madrasa', 'note', 'survey_id'
    ];

    public function survey()
    {
        return $this->belongsTo('App\Survey', 'survey_id');
    }
}
