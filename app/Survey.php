<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = ['house_no', 'family_head_name', 'sure_name', 'age', 'gender', 'occupation', 'political_party', 'blood_group', 'phone'];

    public function family_members()
    {
        return $this->hasMany('App\FamilyMember', 'survey_id');
    }
}
